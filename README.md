Calculator
---
Simple React + Redux calculator with Express api server.

The project
---
This project is a simple React + Redux calculator with a2b capabilities app. - running on port 3000<br>
The Api server is an express app (src/server.js) - running on port 3001<br>
proxy is used to connect to the server thus calling `/a2b` will be forwareded to the express server.<br>
For convenience the app has a very simple async loading mechanizm implemented in the main App constructor.<br>

Client
---
Client policy config is beeing fetch by App component.

Server
---
Currently there is a single route (src/server/routes/index.js) POST `\a2b, any other routes are welcome under routes dir.


API
---

POST `\a2b`
---
Query Params: country, optional
Body: clientPolicy
Return:
    200: new client policy object.
    400: error message.

How to run the project
---
```shell
git clone https://bitbucket.org/pinikeizman/ironesourcec
cd calculator
npm install
npm start
```