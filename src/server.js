const express = require('express')
const app = express()

// config
const appConfig = require('./server/config/main-config');
const routeConfig = require('./server/config/route-config');

appConfig.init(app);
routeConfig.init(app);

app.listen(3001, () => console.log('Example app listening on port 3001!'));