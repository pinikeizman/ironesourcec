const express = require('express');
const router = express.Router();

const a2bController = require('../controllers/a2b');

router.post('/a2b',function (req, res, next) {

  const clientPolicy = req.body;
  const country = req.query.country || "";

  a2bController.A2BPolicy({
    clientPolicy,
    country
  }).then( newPolicy => res.send(newPolicy) )
  .catch(err=>{
    //bad request the only way we can get error from the controller if policy isn't plain object.
    res.status(400);
    res.send({error: "bad request."}) 
  });
});

module.exports = router;
