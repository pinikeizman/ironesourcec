(function(appConfig) {

  'use strict';

  // *** main dependencies *** //
  const path = require('path');
  const bodyParser = require('body-parser');
  const morgan = require('morgan');

 
  
  appConfig.init = function(app) {

    // *** app middleware *** //
    if (process.env.NODE_ENV !== 'test') {
      app.use(morgan('dev'));
    }

    app.use(bodyParser.json());

  };

})(module.exports);
