
const _ = require("lodash");

const getPolicy = (clientData) => {
  
  const userFromUS = clientData.country == "US";
  let calcFirstLine = _.get(clientData.clientPolicy,"order[0]");
  calcFirstLine = calcFirstLine ? calcFirstLine : userFromUS && _.random(1) % 2 == 0 ? ["AC","%","+/-","÷"] : ["AC","+/-","%","÷"];

  const policy = {
      order: [
        calcFirstLine,
        ["7","8","9","x"],
        ["4","5","6","-"],
        ["1","2","3","+"],
        ["0",".","="]
      ],
      "=": {
        bColor:["lizard"],
      },
      "÷": {
        bColor:["orange"],
      },
      "x": {
        bColor:["orange"],
      },
      "-": {
        bColor:["orange"],
      },
      "+": {
        bColor:["orange"],
      },
      '0': {
        wide: true
      }
    }
  return policy;
};

function A2BPolicy(clientData) {
  return new Promise( (resolve, reject) =>{
    // client policy excpected to be plain object.
    let result = {};
    try{
      result = _.merge(clientData.clientPolicy, getPolicy(clientData));
    }catch(err){
      reject(err);
    }
    resolve(result);
  });
}

module.exports = {
  A2BPolicy
};
