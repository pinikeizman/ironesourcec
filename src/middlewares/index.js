import logger from './logger';

const middlewares = [];

if (process.env.NODE_ENV == 'development') {
  middlewares.push(logger);
}

export default middlewares;
