import React from 'react';
import Display from './Display';
import ButtonPanel from './ButtonPanel';
import './App.css';
import { connect } from 'react-redux'
import { calculate, calculatorLoaded } from '../actions/calculator'
import Loading from 'react-loading-animation'
import {asynCall} from '../api/index';
import ls from 'local-storage';

const clientPolicyLocalStorgeKey = "clientPolicy";

export class App extends React.Component {
  state = {
    clientPolicy: ls.get(clientPolicyLocalStorgeKey)
  }

  handleClick = (buttonName) => {
    this.props.calculate(buttonName);
  }

  componentDidMount(){

    let params = (new URL(document.location)).searchParams;
    let name = params.get("country");
    
    asynCall(`/a2b?country=${name}`,{
        method:"POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify( this.state.clientPolicy || {} )
      }).then( clientPolicy => {
        ls.set(clientPolicyLocalStorgeKey, clientPolicy);
        this.setState({
          clientPolicy
        })
      }).catch( err => {
        console.log("That is not going to hapen...");
      }).then( () => this.props.calculatorLoaded() );

  }

  render() {
    const { total, next, loaded } = this.props;
    return loaded ? (
      <div className="component-app">
        <Display
          value={next || total || '0'}
        />
        <ButtonPanel
          policy={this.state.clientPolicy}
          clickHandler={this.handleClick}
        />
      </div>
    ) : <Loading margin="50%" />;
  }
}

const mapState = state => ({
  total: state.calculator.total,
  next: state.calculator.next,
  loaded: state.calculator.loaded
})
export default connect(mapState, { calculate, calculatorLoaded })(App)
