import Button from './Button';
import React from 'react';
import PropTypes from 'prop-types';
import _ from "lodash";
import './ButtonPanel.css';

class ButtonPanel extends React.Component {
  handleClick = (buttonName) => {
    this.props.clickHandler(buttonName);
  }
  render() {
    const policy = this.props.policy;
    const {order} = policy;
    return (
      <div className="component-button-panel">
        {
          order.map(names => 
            <div key={_.uniqueId()}>
              {
                 names.map(name =>
                  < Button 
                          key={name}
                          name={name}
                          color={_.get(policy[name], "bColor")}
                          wide={_.get(policy[name], "wide")}
                          clickHandler={this.handleClick} />
                )
              }
            </div>
          )
        }
      </div>
    );
  }
}
ButtonPanel.propTypes = {
  clickHandler: PropTypes.func,
};
export default ButtonPanel;
