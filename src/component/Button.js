import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';


class Button extends React.Component {
  handleClick = () => {
    this.props.clickHandler(this.props.name);
  }

  render() {
    
    const className = [
      "component-button",
      this.props.color ? this.props.color : "",
      this.props.wide ? "wide" : "",
    ];

    return (
      <div
        className={className.join(" ").trim()}
        style={this.props.style}
      >
        <button
          onClick={this.handleClick}
        >
          {this.props.name}
        </button>
      </div>
    );
  }
}
Button.propTypes = {
  name: PropTypes.string,
  orange: PropTypes.bool,
  wide: PropTypes.bool,
  clickHandler: PropTypes.func,
};
export default Button;
