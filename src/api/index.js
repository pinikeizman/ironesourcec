

export const asynCall = (url = "/",headers = {})=>{

  return fetch(url,headers)
    .then(response => {
      if(response.status != 200){
        throw new Error("request failed");
      }
      if(response.status == 204){
        return {};
      }
      return response.json();
    });

}